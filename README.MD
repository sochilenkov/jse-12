# TASK MANAGER

## DEVELOPER INFO

* **Name**: Nikita Sochilenkov

* **E-mail (main)**: nsochilenkov@t1-consulting.ru
* **E-mail (spare)**: sochilenckov@yandex.ru

## SOFTWARE

* **OS**: Windows 11 Pro 10.0.22621 Build 22621

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: i9 12900k

* **RAM**: 32GB

* **SSD**: NVME 2Tb

## PROGRAM BUILD

```shell
mvn clean install
```

## PROGRAM RUN

```shell
java -jar task-manager.jar
```