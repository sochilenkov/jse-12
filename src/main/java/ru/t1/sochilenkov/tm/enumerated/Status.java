package ru.t1.sochilenkov.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.getDisplayName().equals(value)) return status;
        }
        return null;
    }

    public static String[] getDisplayNames() {
        String[] statusValues = new String[Status.values().length];
        for (final Status status : Status.values()) {
            statusValues[status.ordinal()] = status.displayName;
        }
        return statusValues;
    }

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
